#!/usr/local/bin/python3

# https://github.com/datastax/python-driver
import cassandra.cluster

import functools
import struct
import sys

# https://docs.python.org/3.4/library/struct.html#struct-format-strings
max_str_len = 10
endianness_str = ">"
tuple_formats = {float: "dd",
                 int: "di",
                 str: "d" + str(max_str_len) + "p"}
identity = lambda x: x
encode_conversions = {str: lambda s: s.encode()}
decode_conversions = {str: lambda b: b.decode()}

def check_type_of_values(tuple_list, value_type):
    assert(functools.reduce(lambda res, tup: res and type(tup[1]) is value_type,
                            tuple_list,
                            True))

def convert(tuple_list, conversions, value_type):
    conversion = conversions.get(value_type, identity)
    return list(map(lambda tup: (tup[0], conversion(tup[1])),
                    tuple_list))

def value_list_format(value_type, list_len):
    tuple_format = tuple_formats[value_type]
    return endianness_str + ''.join([tuple_format] * list_len)

def tuple_list_to_bytes(tuple_list):
    value_type = type(tuple_list[0][1])
    check_type_of_values(tuple_list, value_type)
    tuple_list = convert(tuple_list, encode_conversions, value_type)
    value_list = functools.reduce(lambda conc, tup: conc.extend(tup) or conc,
                                  tuple_list, [])
    format_for_list = value_list_format(value_type, len(tuple_list))
    byte_buf = struct.pack(format_for_list, *value_list)
    return byte_buf

def bytes_to_tuple_list(byte_buf, value_type):
    tuple_format = tuple_formats[value_type]
    iterable = struct.iter_unpack(endianness_str + tuple_format, byte_buf)
    tuple_list = convert(iterable, decode_conversions, value_type)
    return tuple_list

def list_arg_to_bytes(arg):
    return (type(arg) is list and tuple_list_to_bytes(arg)) or arg

def insert(session, stmt, args):
    converted_args = map(list_arg_to_bytes, args)
    session.execute(stmt, converted_args)
    
def main():
    keyspace = "cd38757"
    cluster = cassandra.cluster.Cluster()
    session = cluster.connect(keyspace)
    try:
        if len(sys.argv) > 2:
            insert(session, sys.argv[1], eval(sys.argv[2]))
        else:
            print("nothing to do!")
    finally:
        session.shutdown()
        cluster.shutdown()

def test():
    tuple_list = [[0.1, 1.0], [0.2, 2.0], [0.3, 3.0]]
    byte_buf = tuple_list_to_bytes(tuple_list)
    tuple_list2 = bytes_to_tuple_list(byte_buf, float)
    print(tuple_list2)

    tuple_list = [[0.1, "one"], [0.2, "two"], [0.3, "three"]]
    byte_buf = tuple_list_to_bytes(tuple_list)
    tuple_list2 = bytes_to_tuple_list(byte_buf, str)
    print(tuple_list2)

#test()
main()
