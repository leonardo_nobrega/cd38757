#!/usr/local/bin/python3

import json
import sys

def get_key_sequence():
    key_sequence_str = sys.argv[1]
    return key_sequence_str.split(",")

def json2csv(in_file, out_file):
    key_sequence = get_key_sequence()
    in_line = in_file.readline()
    while len(in_line) > 0:
        obj = json.loads(in_line)
        vals = map(lambda k: str(obj[k]), key_sequence)
        out_line = "|".join(vals)
        out_file.write(out_line)
        out_file.write("\n")
        in_line = in_file.readline()
    out_file.close()

json2csv(sys.stdin, sys.stdout)
