#!/bin/sh

IP=$1

~/Documents/code/cd38757/src/count_msgs.py $IP > ip.dat

metrics () {
    cat ip.dat | cut -d " " -f 2 | sort | uniq
}

graph () {
    METRIC=$1
    `echo "set terminal png; \
set output '$METRIC-$IP.png'; \
set title '$METRIC'; \
set xlabel 'minutes'; \
set ylabel 'num msgs'; \
plot '< grep $METRIC ip.dat' using 1:3 notitle lc rgb 'red'" | gnuplot`
}

for m in `metrics`; do
    graph "$m"
done;
