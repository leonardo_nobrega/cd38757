#!/usr/bin/perl

use strict;
use warnings;

my $cols = <<"EOF";
deviceId:integer
deviceName:text
deviceIp:text
objectId:integer
objectName:text
objectDesc:text
indicatorId:integer
indicatorName:text
value:text
format:integer
time:real
clusterName:text
peerId:integer
peerIp:text
pluginId:integer
pluginName:text
EOF

sub pairs_key {
    my $kv = $_[0];
    my @pair = split(/:/, $kv);
    return $pair[0];
}

sub pairs_value {
    my $kv = $_[0];
    my @pair = split(/:/, $kv);
    return $pair[1];
}

sub space_str_pair {
    my $str_pair = $_[0];
    $str_pair =~ y/:/ /;
    return $str_pair;
}

my @str_pairs = split(/\n/, $cols);
my $col_names = join(",", map(pairs_key($_), @str_pairs));

my $create_stmt = "create table messages (" .
    join(", ", map(space_str_pair($_), @str_pairs)) .
    ");";

my $num_rows = $ARGV[0];
my $head_command = "";
if ($num_rows) {
    $head_command = "head -n $num_rows |";
}

my $commands = <<"EOF";
sqlite3 kafka-msgs.db "$create_stmt"
mkfifo tempfifo
gzcat kafka-dump-20170106.csv.gz | $head_command ~/Documents/code/cd38757/src/json2csv.py $col_names > tempfifo &
sqlite3 kafka-msgs.db ".import tempfifo messages"
rm tempfifo
EOF

#print($commands);
exec($commands);
