#!/usr/local/bin/python3

import contextlib
import sqlite3
import sys

DB = "kafka-msgs.db"

START_TIME = 'select time from vcp_poc where deviceIp = "{}" order by time limit 1;'

END_TIME = """select time from vcp_poc
where deviceIp = "{}"
order by time desc
limit 1;"""

MSG_COUNTS = """select indicatorName, count(*) from vcp_poc
where deviceIp = "{}"
and time > {} and time < {}
group by indicatorName;"""

# increments of 60 seconds
TIME_INCREMENT = 60

def get_time(conn, stmt, ip):
    with contextlib.closing(conn.cursor()) as cur:
        cur.execute(stmt.format(ip))
        row = cur.fetchone()
        time_float = row[0];
        return int(time_float)

def get_start_time(conn, ip):
    return get_time(conn, START_TIME, ip)

def get_end_time(conn, ip, start_time, num_minutes_str):
    last_time_for_ip = get_time(conn, END_TIME, ip)
    return (num_minutes_str and
            start_time + int(num_minutes_str) * TIME_INCREMENT) or last_time_for_ip

def print_row(time, row):
    row_copy = list(row)
    row_copy.insert(0, time)
    row_copy = map(str, row_copy)
    row_str = ' '.join(row_copy)
    print(row_str)

def count_msgs(ip, num_minutes_str):
    with sqlite3.connect(DB) as conn:
        with contextlib.closing(conn.cursor()) as cur:
            start_time = get_start_time(conn, ip)
            end_time = get_end_time(conn, ip, start_time, num_minutes_str)
            time = start_time
            minutes = 0
            while time < end_time:
                stmt = MSG_COUNTS.format(ip, time, time + TIME_INCREMENT)
                for row in cur.execute(stmt):
                    print_row(minutes, row)
                time = time + TIME_INCREMENT
                minutes = minutes + 1
                print("{}/{}".format(time - start_time, end_time - start_time),
                      file=sys.stderr)

num_minutes_str = None
if len(sys.argv) > 2:
    num_minutes_str = sys.argv[2]
count_msgs(sys.argv[1], num_minutes_str)
